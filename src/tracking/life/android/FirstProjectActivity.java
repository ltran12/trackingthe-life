package tracking.life.android;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;

public class FirstProjectActivity extends Service implements LocationListener {
	private final int MAX_TIME = 10000;
	// private final int MAX_TIME = 5000;
	private static final int TWO_MINUTES = 60 * 2 * 1000;
	private PointOfInterestManager POIManager;
	private String provider;
	LocationManager locationManager;
	Location prevLoc;
	Geocoder geocoder;
	private POIHistory PHis;
	Location lastLocation;
	private int timeRefreshGPS = TWO_MINUTES;

	private String mem;
	private long memberID;

	public void onCreate() {

		Criteria criteria = new Criteria();
		
		//criteria.setAccuracy(Criteria.ACCURACY_HIGH);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		criteria.setSpeedRequired(false);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		// POIManager = new PointOfInterestManager(this, memberID);

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		provider = locationManager.getBestProvider(criteria, true);
		// provider = locationManager.GPS_PROVIDER;
		lastLocation = locationManager.getLastKnownLocation(provider);
		locationManager.requestLocationUpdates(provider, timeRefreshGPS, 0,
				this);

		if (lastLocation != null) {
			PHis = new POIHistory(new PointOfInterest(lastLocation));

			onLocationChanged(lastLocation);
		} else
			PHis = new POIHistory(new PointOfInterest(new Location("null")));

	}

	public void onStart(Intent intent, int startid) {

		mem = intent.getStringExtra("mem");
		System.out.println("mem ne: " + mem);
		memberID = Long.parseLong(mem);
		POIManager = new PointOfInterestManager(this, memberID);

		locationManager.requestLocationUpdates(provider, timeRefreshGPS, 0,
				this);
	}

	public void onDestroy() {
		locationManager.removeUpdates(this);

	}

	public void onLocationChanged(Location location) {
		check(new PointOfInterest(location));
	}

	public void onProviderDisabled(String provider) {

	}

	public void onProviderEnabled(String provider) {

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_HIGH);
		criteria.setPowerRequirement(Criteria.POWER_LOW);
		criteria.setSpeedRequired(false);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);

		if (status == LocationProvider.TEMPORARILY_UNAVAILABLE
				|| status == LocationProvider.OUT_OF_SERVICE) {

			check(new PointOfInterest(
					locationManager.getLastKnownLocation(provider)));
		}
		provider = locationManager.getBestProvider(criteria, true);

		locationManager.requestLocationUpdates(provider, timeRefreshGPS, 0,
				this);
		PHis = new POIHistory(new PointOfInterest(
				locationManager.getLastKnownLocation(provider)));
	}

	/*
	 * keep adding to a sentinel point of interest then the data will be push to
	 * the database when user change
	 */
	int count;

	public void check(PointOfInterest point) {
		// get the different between the point we want to add in to the system
		// and the current time of the system
		long timeStay = point.getLocation().getTime()
				- PHis.getPointOfInterst().getLocation().getTime();
		System.out.println("timeStay = " + timeStay + " max time = " + MAX_TIME
				+ "  point get time = " + point.getLocation().getTime()
				+ " PHIS get time = "
				+ PHis.getPointOfInterst().getLocation().getTime());
		if (PHis.getPointOfInterst().equals(point) && timeStay < MAX_TIME) {
			// add the new time to the POI history if it's in the same
			// location
			// System.out.println("not suppose to be here");
			PHis.setLengthVisit(timeStay);
		} else {
			if (timeStay >= MAX_TIME) {
				// System.out.println("this is a check !!11111111111111111111111");

				POIManager.addPOIHistory(PHis);
			}

			// reset a PHis
			PHis = new POIHistory(point);
			// System.out.println("weird w/o 111");
		}

	}

	public IBinder onBind(Intent intent) {
		return null;
	}
}