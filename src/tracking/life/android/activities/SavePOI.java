package tracking.life.android.activities;

import java.util.ArrayList;

import tracking.life.android.PointOfInterestManager;
import android.R.anim;
import android.R.color;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class SavePOI extends Activity {
	private long memberID;
	private long locID;
	private PointOfInterestManager manager;
	private Location loc;
	private String test;

	Button but,butCancel;
	EditText etext;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// Intent i = getIntent();
		// test = i.getStringExtra("mem");
		// memberID = Long.parseLong(test);
		setContentView(tracking.life.android.R.layout.save_poi);
		

		but = (Button) findViewById(tracking.life.android.R.id.buttonS);
		butCancel = (Button) findViewById(tracking.life.android.R.id.buttonCancelSavePoi);
		etext = (EditText) findViewById(tracking.life.android.R.id.editTextLocation);

		but.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				if (etext.toString().trim().equals("")) {
					toast();
				} else {
					Intent in = new Intent();
					in.putExtra("POIName", etext.getText().toString());
					setResult(RESULT_OK, in);
					finish();
				}
			}
		});
		
		butCancel.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent();
				String task = null;
				in.putExtra("TaskList", task);
				setResult(RESULT_CANCELED, in);
				finish();
			}
		});
	}

	public void toast() {
		Toast.makeText(this, "My location is : ", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onBackPressed() {
		butCancel.performClick();
	}
}
