package tracking.life.android.activities;

import java.util.ArrayList;

import tracking.life.android.PointOfInterestManager;
import android.R.anim;
import android.R.color;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class POIListView extends Activity {
	private long memberID;
	private long locID;
	private PointOfInterestManager manager;
	private Location loc;
	private String test;

	ScrollView scroll;
	TableLayout table;
	LinearLayout linear;
	TableRow row;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		test = i.getStringExtra("mem");
		memberID = Long.parseLong(test);

		manager = new PointOfInterestManager(this, memberID);
		createView();
		setContentView(linear);
	}

	public void createView() {
		ArrayList<String> temp = manager.listLocation();

		scroll = new ScrollView(this);
		table = new TableLayout(this);
		scroll.addView(table);
		linear = new LinearLayout(this);
		linear.addView(scroll);
		linear.setOrientation(LinearLayout.VERTICAL);
		Button but = new Button(this);
		but.setText("OK");
		linear.addView(but);

		if (temp.size() > 0) {
			int count = 0;

			for (String st : temp) {
				row = new TableRow(this);
				table.addView(row);

				TextView text = new TextView(this);
				if (count % 2 == 1){
					
					row.setBackgroundColor(Color.BLUE);
				}
				else
				row.setBackgroundColor(Color.WHITE);
				
				row.addView(text);
				
				text.setText(st);
				if (count % 2 == 1) text.setTextColor(Color.WHITE);
				else if (count % 2 == 0)  text.setTextColor(Color.BLACK);
				count++;

			}
			row.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					
				}
			});
		} else {
			TextView text = new TextView(this);
			linear.addView(text);
			text.setText("There are no note for this user at this location");
		}


		but.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent in = new Intent();
				setResult(RESULT_OK, in);
				finish();

			}
		});

	}
}
