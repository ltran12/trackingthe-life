package tracking.life.android.activities;

import java.util.ArrayList;

import tracking.life.android.PointOfInterestManager;
import android.R.color;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AboutUs extends Activity {
	ScrollView scroll;
	TableLayout table;
	LinearLayout linear;
	String test;
	TextView text;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		createView();
		setContentView(linear);
	}

	public void createView() {

		scroll = new ScrollView(this);
		table = new TableLayout(this);
		scroll.addView(table);
		linear = new LinearLayout(this);
		linear.setOrientation(LinearLayout.VERTICAL);
		linear.addView(scroll);

		scroll.setBackgroundColor(Color.BLUE);

			text = new TextView(this);
			linear.addView(text);
			text.setText(" Tracking the Life\n\n Illinois Institute of Technology          \n CS 487:Software Engineering\n Course Project\n\n Team Members:\n\n Firat Karakusoglu\n Lam Tran\n Dwayne Charp\n Hanuel Moon\n Bir Kafle");

		Button but = new Button(this);
		but.setText("OK");
		linear.addView(but);
		but.setId(100);
		but.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent in = new Intent();
				setResult(RESULT_OK, in);
				finish();
			}
		});
	}
	
	@Override
	public void onBackPressed() {
	   Button butOk = (Button)findViewById(100);
	   butOk.performClick();
	}
}
