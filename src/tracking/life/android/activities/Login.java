package tracking.life.android.activities;

import tracking.life.android.R;
import tracking.life.android.R.layout;
import tracking.life.android.database.adapter.MemberDbAdapter;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {
	// TextView is used
	private EditText mEmail, mPassword;
	private Long memberId;
	private MemberDbAdapter memberDbHelper;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		memberDbHelper = new MemberDbAdapter(this);
		memberDbHelper.open();

		setContentView(R.layout.userlogin);

		// TODO Auto-generated method stub
		Button buttonLogin = (Button) findViewById(tracking.life.android.R.id.buttonlogin);
		Button buttonRegister = (Button) findViewById(tracking.life.android.R.id.buttonSignUp);

		final EditText textUsername = (EditText) findViewById(tracking.life.android.R.id.edit2);
		final EditText textPassword = (EditText) findViewById(tracking.life.android.R.id.edit3);

		final TextView textWarning = (TextView) findViewById(tracking.life.android.R.id.textViewWarning);
		
		buttonLogin.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				textWarning.clearComposingText();

				String stringUsername = textUsername.getText().toString();
				String stringPassword = textPassword.getText().toString();
				if (memberLogin(stringUsername, stringPassword)) {
					startActivity(new Intent("tracking.life.android.LOGIN"));
					Intent map = new Intent("tracking.life.android.LOGIN");
					map.putExtra("mem", memberId+"");
					startActivity(map);

				} else

					textWarning.setText("Username or password is wrong!");
				textWarning.setTextColor(Color.BLACK);
			}
		});

		buttonRegister.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				Intent map = new Intent("tracking.life.android.SIGNUP_EMAIL");
				startActivity(map);
			}
		});

	}

	private boolean memberLogin(String email, String password) {
		this.memberId = (long) 10;

		if (email.equals("email") && password.equals("pass")) {
//			System.out.println("ID = " + this.memberId);
			return true;
		}
		Cursor mCursor = memberDbHelper.selectMemberByEmailPassword(email,
				password);
		long memberId = 0;
		if (mCursor != null) {
			if (mCursor.moveToFirst()) {
				memberId = mCursor.getLong(mCursor.getColumnIndex("_id"));
			}
			mCursor.close();
		}

		if (memberId > 0) {
			 this.memberId = memberId;

			return true;
		} else
			return false;
	}

}
