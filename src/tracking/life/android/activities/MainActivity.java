package tracking.life.android.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import tracking.life.android.FirstProjectActivity;
import tracking.life.android.GpsAlarm;
import tracking.life.android.GpsAlarmManager;
import tracking.life.android.GpsProximityIntentReceiver;
import tracking.life.android.MapOverlay;
import tracking.life.android.MyMapView;
import tracking.life.android.PointOfInterest;
import tracking.life.android.PointOfInterestManager;
import tracking.life.android.R;
import tracking.life.android.TTLMapController;
import tracking.life.android.database.adapter.LocationHistoryDbAdapter;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends MapActivity {
	private long memberID;
	String mem;
	private EditText text;
	private static final int latitudeE6 = 37985339;
	private static final int longitudeE6 = 23716735;

	private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 1; // in
																		// Meters
	private static final long MINIMUM_TIME_BETWEEN_UPDATE = 1000; // in
																	// Milliseconds
	private static final long POINT_RADIUS = 10; // in Meters
	private static final long PROX_ALERT_EXPIRATION = -1;
	private static final String PROX_ALERT_INTENT = "tracking.life.android.ProximityAlert";
	private boolean toggleON;
	private final int defaultZoom = 10;

	private MyMapView mapView;
	private LocationManager locationManager;
	private TTLMapController mapController;
	private PointOfInterestManager POIManager;
	private GpsAlarmManager alarmManager;
	private Drawable drawable;
	private MapOverlay itemizedOverlay;

	private List<Overlay> mapOverlays;
	private LocationHistoryDbAdapter lo;
	private String POITitle;
	private String newTask;
	private long POIId;

	public void onCreate(Bundle savedInstanceState) {
		Intent i = getIntent();
		mem = i.getStringExtra("mem");
		System.out.println(mem);
		memberID = Long.parseLong(mem);

		super.onCreate(savedInstanceState);
		setContentView(tracking.life.android.R.layout.map);

		toggleON = true;

//		Debug.startMethodTracing();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				MINIMUM_TIME_BETWEEN_UPDATE, MINIMUM_DISTANCECHANGE_FOR_UPDATE,
				new MyLocationListener());

		POIManager = new PointOfInterestManager(this, memberID);
		alarmManager = new GpsAlarmManager(this, memberID);

		mapView = (MyMapView) findViewById(R.id.mapView);
		mapView.setBuiltInZoomControls(true);

		drawable = this.getResources().getDrawable(R.drawable.locationicon1);

		mapOverlays = mapView.getOverlays();
		reloadIntents();

		itemizedOverlay = new MapOverlay(drawable, this);
		itemizedOverlay.setPOIManager(POIManager);

		mapController = new TTLMapController(mapView.getController());
		// mapController.intializeMap(defaultZoom, point);

		Intent in = new Intent("android.intent.action.Service");
		in.putExtra("mem", mem);
		startService(in);

		ImageButton note = (ImageButton) findViewById(tracking.life.android.R.id.ib1);
		ImageButton task = (ImageButton) findViewById(tracking.life.android.R.id.ib2);
		ImageButton stat = (ImageButton) findViewById(tracking.life.android.R.id.ib3);
		MapView map = (MapView) findViewById(tracking.life.android.R.id.mapView);
		ImageButton addPOIButton = (ImageButton) findViewById(tracking.life.android.R.id.saveL);
		ImageButton addTask = (ImageButton) findViewById(tracking.life.android.R.id.aTask);
		ImageButton addNote = (ImageButton) findViewById(tracking.life.android.R.id.aNote);
		ImageButton POIBut = (ImageButton) findViewById(tracking.life.android.R.id.POI);

		ImageButton myLocation = (ImageButton) findViewById(tracking.life.android.R.id.imageView1);

		ImageButton toggleOverlays = (ImageButton) findViewById(tracking.life.android.R.id.setting1);
//		ImageButton setting = (ImageButton) findViewById(tracking.life.android.R.id.setting);

		stat.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				Intent noteIntent = new Intent("tracking.life.android.STAT");
				noteIntent.putExtra("mem", mem);
				startActivityForResult(noteIntent, 0);
			}
		});
		POIBut.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				Intent noteIntent = new Intent("tracking.life.android.POIList");
				noteIntent.putExtra("mem", mem);
				startActivityForResult(noteIntent, 0);
			}
		});

		task.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent noteIntent = new Intent("tracking.life.android.TASK");
				noteIntent.putExtra("mem", mem);
				startActivityForResult(noteIntent, 1);

			}
		});

		map.setBuiltInZoomControls(true);

		note.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent noteIntent = new Intent("tracking.life.android.NOTE");
				noteIntent.putExtra("mem", mem);
				startActivityForResult(noteIntent, 0);
			}
		});

//		setting.setOnClickListener(new View.OnClickListener() {
//
//			public void onClick(View v) {
//
//				startActivity(new Intent("tracking.life.android.SETTING"));
//
//			}
//		});

		addPOIButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Location location = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);

				if (location == null) {
					printLocationNF();
				}

				else {

					PointOfInterest po = POIManager.doesPOIExist(location);
					if (po == null) {
						Intent noteIntent = new Intent(
								"tracking.life.android.savePOI");
						// noteIntent.putExtra("action", "nothing");
						startActivityForResult(noteIntent, 0);
					} else
						addPOI();
				}
			}
		});

		addTask.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Location location = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);

				if (location == null) {
					printLocationNF();
				}

				else {

					PointOfInterest po = POIManager.doesPOIExist(location);
					if (po != null) {
						Intent noteIntent = new Intent(
								"tracking.life.android.saveTask");
						startActivityForResult(noteIntent, 0);
					} else {
						printToast2();
					}
				}
			}
		});

		addNote.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Location location = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);

				if (location == null) {
					printLocationNF();
				}

				else {
					PointOfInterest po = POIManager.doesPOIExist(location);
					if (po != null) {
						Intent noteIntent = new Intent(
								"tracking.life.android.saveNote");
						startActivityForResult(noteIntent, 0);
					} else {
						printToast2();
					}
				}
			}
		});

		toggleOverlays.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				toggleOverLaysOnMap();
			}
		});

		myLocation.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				displayCoordinates();
			}
		});

	}

	protected boolean isRouteDisplayed() {
		return false;
	}

	/******************** Gps Proximity alert section *****************/

	private void saveProximityAlertPoint(long locationID) {
		addProximityAlert(locationID, "I need to by some grapes");

	}

	private void addPOI() {

		long id = 0;

		Location location = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (location == null) {
			Toast.makeText(this, "Not saved", Toast.LENGTH_LONG).show();
			return;
		}
		Toast.makeText(
				this,
				"My location is : " + location.getLatitude() + " "
						+ location.getLongitude(), Toast.LENGTH_LONG).show();

		id = POIManager.addPOI(location, POITitle);

		GeoPoint point = POIManager.convertLocationToPoint(location);

		POIManager.getPOIWithId(id).setGeoPoint(point);

		mapController.animateTo(point);

		mapController.setZoom(defaultZoom);
		addOverlayWithPOI(POIManager.getPOIWithId(id));
		mapView.invalidate();
		Log.v("save", "ID value:" + id);

	}

	private void displayCoordinates() {
		Location location = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (location != null) {
			Toast.makeText(
					this,
					"My location is : " + location.getLatitude() + " "
							+ location.getLongitude(), Toast.LENGTH_LONG)
					.show();
			GeoPoint point = new GeoPoint(
					PointOfInterestManager.convertTo1E6(location.getLatitude()),
					PointOfInterestManager
							.convertTo1E6(location.getLongitude()));

			mapController.animateTo(point);
			mapController.setZoom(6);
		}

		else {
			Toast.makeText(this,
					"Can not access GPS device. Please make sure GPS is on",
					Toast.LENGTH_LONG).show();
		}
	}

	private void addProximityAlert(long locationID, String task) {

		long alarmId = alarmManager.addAlarm(locationID, task);
		Log.v("before set intent", "alarmID value:" + alarmId);
		if (alarmId != -1)
			alarmManager.setIntent(alarmId, createIntent(alarmId, locationID));

	}

	private Intent createIntent(long alarmID, long locationID) {

		Location location = POIManager.getLocationWithID(locationID);

		String uniqueIntentString = new String(PROX_ALERT_INTENT + locationID);

		Intent intent = new Intent(uniqueIntentString);

		intent.putExtra("locationID", locationID);
		intent.putExtra("alarmID", alarmID);
		intent.putExtra("mem", Long.parseLong(mem));
	
				

		PendingIntent proximityIntent = PendingIntent.getBroadcast(this, 0,
				intent, 0);

		locationManager.addProximityAlert(location.getLatitude(), // the
																	// latitude
																	// of the
																	// central
																	// point of
																	// the alert
																	// region
				location.getLongitude(), // the longitude of the central point
											// of the alert region
				POINT_RADIUS, // the radius of the central point of the alert
								// region, in meters
				PROX_ALERT_EXPIRATION, // time for this proximity alert, in
										// milliseconds, or -1 to indicate no
										// expiration
				proximityIntent // will be used to generate an Intent to fire
								// when entry to or exit from the alert region
								// is detected
				);

		IntentFilter filter = new IntentFilter(uniqueIntentString);
		registerReceiver(new GpsProximityIntentReceiver(), filter);

		return intent;
	}

	public void reloadIntents() {
		ArrayList<GpsAlarm> Alarms = alarmManager.getListOfAlarms();
		for (GpsAlarm alarm : Alarms) {
			Log.v("from reload", "locationId" + alarm.getLocationId());
			createIntent(alarm.getAlarmId(), alarm.getLocationId());
		}
	}

	public void toggleOverLaysOnMap() {

		if (toggleON) {
			updateOverlaysOnMap();
		}

		else {
			toggleON = true;
			mapOverlays.clear();
			Log.v("from toggle on ", " clear overlays");
			mapView.invalidate();
		}

	}

	public void updateOverlaysOnMap() {
		for (PointOfInterest poi : POIManager.PointsOfInterests) {
			addOverlayWithPOI(poi);
			toggleON = false;
		}

		mapView.invalidate();
	}

	public void addOverlayWithPOI(PointOfInterest poi) {

		OverlayItem item = itemizedOverlay.createOverlayItem(
				PointOfInterestManager.convertTo1E6(poi.getLocation()
						.getLatitude()), PointOfInterestManager
						.convertTo1E6(poi.getLocation().getLongitude()), poi
						.getPOIName(), "Task info will go here");
		poi.setOverlayItem(item);
		itemizedOverlay.addOverlay(item);
		mapOverlays.add(itemizedOverlay);
	}

	private String newNote;

	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		POITitle = null;
		newTask = null;
		newNote = null;
		super.onActivityResult(requestCode, resultCode, intent);
		Bundle extras = intent.getExtras();
		if (extras != null) {
			POITitle = extras.getString("POIName");
			newTask = extras.getString("TaskList");
			newNote = extras.getString("NoteList");
			if (POITitle != null)
				addPOI();
			if (newTask != null) {
				Location location = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);

				long id = POIManager.addPOI(location, "unknown");
				addProximityAlert(id, newTask);
			}

			if (newNote != null) {
				Location location = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				POIManager.addNote(location, newNote);
				System.out.println("note = " + newNote);

			}
		}
	}

	private void printLocationNF() {
		Toast.makeText(this, "Location not found", Toast.LENGTH_LONG).show();

	}

	private void printToast2() {
		Toast.makeText(this,
				"Please save this location before adding note/task",
				Toast.LENGTH_LONG).show();
	}

	public class MyLocationListener implements LocationListener {
		public void onLocationChanged(Location location) {

		}

		public void onStatusChanged(String s, int i, Bundle b) {
		}

		public void onProviderDisabled(String s) {
		}

		public void onProviderEnabled(String s) {
		}
	}
	
	@Override
	public void onBackPressed() {
	   return;
	}

	// Create the menu based on the XML defintion
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.listmenu, menu);
			return true;
		} 

		// Reaction to the menu selection
//		@Override
//		public boolean onMenuItemSelected(int featureId, MenuItem item) {
//			switch (item.getItemId()) {
//			case R.id.aboutus:
//				DisplayAboutUs();
//				break;
//			}
//			return super.onMenuItemSelected(featureId, item);
//		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case R.id.aboutus:
				DisplayAboutUs();
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	
		public void DisplayAboutUs() {
			Intent aboutUsIntent = new Intent("tracking.life.android.ABOUTUS");
			startActivityForResult(aboutUsIntent, 0);
		}

}
