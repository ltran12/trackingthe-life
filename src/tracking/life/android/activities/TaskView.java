package tracking.life.android.activities;

import java.util.ArrayList;

import tracking.life.android.PointOfInterestManager;
import android.R.color;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class TaskView extends Activity {
	private long memberID;
	private long locID;
	private PointOfInterestManager manager;
	private Location loc;
	ScrollView scroll;
	TableLayout table;
	LinearLayout linear;
	String test;
	TextView text;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		test = i.getStringExtra("mem");
		memberID = Long.parseLong(test);
		System.out.println("member : " + memberID);
		manager = new PointOfInterestManager(this, memberID);

		createView();
		setContentView(linear);
	}

	public void createView() {

		ArrayList<String> temp = manager.listTaskByLocation(loc);

		scroll = new ScrollView(this);
		table = new TableLayout(this);
		scroll.addView(table);
		linear = new LinearLayout(this);
		linear.setOrientation(LinearLayout.VERTICAL);
		linear.addView(scroll);

		scroll.setBackgroundColor(Color.BLUE);
		int count = 0;
		if (temp.size() > 0) {
			for (String st : temp) {
				

				TableRow row = new TableRow(this);
				table.addView(row);

				LinearLayout li = new LinearLayout(this);
				row.addView(li);

				text = new TextView(this);
				row.addView(text);
				row.setPadding(5, 10, 5, 10);
				if (count % 2 == 1) row.setBackgroundColor(Color.BLUE);
				else if (count % 2 == 0)  row.setBackgroundColor(Color.WHITE);
				text.setText(st);
				if (count % 2 == 1) text.setTextColor(Color.WHITE);
				else if (count % 2 == 0)  text.setTextColor(Color.BLACK);
				count++;

			}
		} else {
			text = new TextView(this);
			linear.addView(text);
			text.setText("There are no tasks at this location");
		}

		Button but = new Button(this);
		but.setText("OK");
		linear.addView(but);
		but.setId(100);
		but.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent in = new Intent();
				setResult(RESULT_OK, in);
				finish();
			}
		});
	}
	
	@Override
	public void onBackPressed() {
	   Button butOk = (Button)findViewById(100);
	   butOk.performClick();
	}
}
