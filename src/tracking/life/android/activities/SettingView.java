package tracking.life.android.activities;

import tracking.life.android.PointOfInterestManager;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.widget.Button;

public class SettingView extends Activity {
	private long memberID;
	private long locID;
	private PointOfInterestManager manager;
	private Location loc;
	private String test;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(tracking.life.android.R.layout.systemsettings);

	}

	@Override
	public void onBackPressed() {
	   Button butOk = (Button)findViewById(100);
	   butOk.performClick();
	}
}
