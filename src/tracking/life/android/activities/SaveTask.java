package tracking.life.android.activities;

import tracking.life.android.PointOfInterestManager;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SaveTask extends Activity {

	Button but, butCancel;
	EditText etext;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(tracking.life.android.R.layout.save_task);

		but = (Button) findViewById(tracking.life.android.R.id.buttonSaveTask);
		butCancel = (Button) findViewById(tracking.life.android.R.id.buttonCancelSaveTask);
		etext = (EditText) findViewById(tracking.life.android.R.id.SaveTask);

		but.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				if (etext.toString().trim().equals("")) {
					toast();
				} else {
					Intent in = new Intent();
					in.putExtra("TaskList", etext.getText().toString());
					setResult(RESULT_OK, in);
					finish();
				}
			}
		});
		
		butCancel.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in = new Intent();
				String task = null;
				in.putExtra("TaskList", task);
				setResult(RESULT_CANCELED, in);
				finish();
			}
		});
		
	}

	public void toast() {
		Toast.makeText(this, "My location is : ", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onBackPressed() {
		butCancel.performClick();
	}
}
