package tracking.life.android;


import java.util.ArrayList;
import android.content.Intent;
import android.location.Location;

public class GpsAlarm {
		
	private ArrayList<Location> alarmLocations;
	private ArrayList<String>   alarmMessages; 
	private long alarmID ;
	private Intent intent; 
	private long locationId; 
	
	public GpsAlarm(long id,long locationId,String task){
		alarmLocations = new ArrayList<Location>();
		alarmMessages = new ArrayList<String>();
		intent = null;
		//alarmLocations.add(location);
		alarmID = id;
		this.locationId = locationId;
		this.addNote(task);
	}
	
	public long getAlarmId(){
		return alarmID;
	}
	public void removeAlarmLocation(Location location){
		alarmLocations.remove(location);
	}
	
	public void addAlarmLocation(Location location){
		alarmLocations.add(location);
	}

	public void addNote(String note){
		
		// still need to add to db 
		alarmMessages.add(note);
	}
	
	public ArrayList<Location> getAlarmLocations(){
		return alarmLocations;
	}
	
	public ArrayList<String> getAlarmMessages(){
		return alarmMessages;
	}
	
	public String getAlarmMessagesInStringFormat(){
		String tasks = "";
		
		for(String task: alarmMessages){
			tasks = tasks + "\n" + task;
		}
		
		return tasks;
	}
	
	public void setPendingIntent(Intent intent){
		this.intent = intent;
	}
	
	public Intent getPendingIntent(){
			return intent;
	}
	
	public long getLocationId(){
		return this.locationId;
	}
}
