package tracking.life.android;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;

public class TTLMapController  {
	MapController mapController;
	
	public TTLMapController(MapController mc){
		mapController = mc;
	}
	
	public void intializeMap(int zoom,GeoPoint point){
		
	}
	
	public void animateTo(GeoPoint point){
		mapController.animateTo(point);
	}
	
	public void setZoom(int zoom){
		mapController.setZoom(zoom);
	}
}
