package tracking.life.android;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

import android.location.Location;

public class PointOfInterest {

	private String name;
	
	private long id;
	private GeoPoint POICoordinates;
	private Location location;
	
	private OverlayItem item;
	public PointOfInterest() {
	 location = new Location("");
	}

	public PointOfInterest(Location location) {
		this.location = location;
		
	}

	public void setPOIName(String newName) {
		this.name = newName;
	}

	public String getPOIName() {
		return this.name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	public void setGeoPoint(GeoPoint point) {
		this.POICoordinates = point;
	}

	public GeoPoint getGeoPoint() {
		return this.POICoordinates;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location loc) {
		location = loc;
	}
	
	public void setLatitude(double lat){
		location.setLatitude(lat);
	}
	
	
	public void setLongitude(double longitude){
		location.setLongitude(longitude) ;
	}
	
	
	
	public void setOverlayItem(OverlayItem item){
		this.item = item;
	}
	
	
	public OverlayItem getOverlayItem(){
		return this.item ;
	}
	
	
	public boolean equals(PointOfInterest otherLoc) {
		if (this.getLocation().distanceTo(otherLoc.getLocation()) < 50) {
			return true;
		}
		return false;
	}

}
