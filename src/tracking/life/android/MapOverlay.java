package tracking.life.android;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;


public class MapOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> mapOverlays = new ArrayList<OverlayItem>();
	
	private PointOfInterest currentPOI;
	private PointOfInterest priorPOI;
	private PointOfInterestManager POIManager = null;
	private GeoPoint point;
	
	Paint mInnerPaint;
	Paint mBorderPaint; 
	Paint mTextPaint; 
	
	String popUpString = null;
	
	Boolean popIsInView = false; 
	
	public MapOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
		
	}

	public  MapOverlay(Drawable defaultMarker, Context context){
		this(defaultMarker);
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		return mapOverlays.get(i);
	}

	@Override
	public int size() {
		return mapOverlays.size();
	}
	
	public void addOverlay(OverlayItem overlay){
		mapOverlays.add(overlay);
		this.populate();
		
	}
	protected boolean onTap(int index){
		Log.v("from ontap",	"in this biatch");
		if(index < 0)
			return false;
		
		Log.v("from ontap",	"index was not less than 0");
		
		OverlayItem item = mapOverlays.get(index);
		long id =	POIManager.findLoctionWithOverlayItem(item);
        currentPOI  = POIManager.getPOIWithId(id);
	
		if(currentPOI != null){
			if(currentPOI != priorPOI){
				popUpString = currentPOI.getPOIName();
				point = currentPOI.getGeoPoint();
				popIsInView = true;
				Log.v("from ontap", "found overlay with this item");
				priorPOI = currentPOI;
				return true;
			}
			else{// remove info box from map
				
				popUpString = null;
				point = null;
				popIsInView = false; 
				priorPOI = null;
			}
			
		}
	    	
		else {
			Log.v("from ontap", "could not find overlay with this item");
		    popIsInView = false;
		}
	
		return true;
	}
	
		public OverlayItem createOverlayItem(int latitude,int longitude, String title, String message){
		GeoPoint point = new GeoPoint(latitude, longitude);
        OverlayItem overlayitem = 
             new OverlayItem(point, title, message);
     
        return overlayitem;
	}
	
	 
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		
		if(popIsInView){
			//	Log.v("from on draw", "string is valid");	
				drawInfoWindow(canvas,  mapView,shadow);			
			}
		
		
	    super.draw(canvas, mapView, shadow);
	}
	
	private void drawInfoWindow(Canvas canvas, MapView mapView, boolean shadow){
		
			if(point != null){
		
				Point selDestinationOffset = new Point();
				
				mapView.getProjection().toPixels(point, selDestinationOffset);
		    	
		    	//  Setup the info window
				int INFO_WINDOW_WIDTH = 175;
				int INFO_WINDOW_HEIGHT = 40;
				RectF infoWindowRect = new RectF(0,0,INFO_WINDOW_WIDTH,INFO_WINDOW_HEIGHT);				
				int infoWindowOffsetX = selDestinationOffset.x-INFO_WINDOW_WIDTH/2;
				int infoWindowOffsetY = selDestinationOffset.y-INFO_WINDOW_HEIGHT- 70;
				infoWindowRect.offset(infoWindowOffsetX,infoWindowOffsetY);
	
				//  Drawing the inner info window
				canvas.drawRoundRect(infoWindowRect, 5, 5, getmInnerPaint());
				
				//  Drawing the border for info window
				canvas.drawRoundRect(infoWindowRect, 5, 5, getmBorderPaint());
					
				//  Draw the MapLocation's name
				int TEXT_OFFSET_X = 10;
				int TEXT_OFFSET_Y = 15;
				canvas.drawText(popUpString,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y,getmTextPaint());
			}	
	}
	
	public Paint getmInnerPaint() {
		
		if ( mInnerPaint == null) {
			mInnerPaint = new Paint();
			mInnerPaint.setARGB(225, 50, 50, 50); //inner color
			mInnerPaint.setAntiAlias(true);
		}
		return mInnerPaint;
	}

	public Paint getmBorderPaint() {
		if ( mBorderPaint == null) {
			mBorderPaint = new Paint();
			mBorderPaint.setARGB(255, 255, 255, 255);
			mBorderPaint.setAntiAlias(true);
			mBorderPaint.setStyle(Style.STROKE);
			mBorderPaint.setStrokeWidth(2);
		}
		return mBorderPaint;
	}

	public Paint getmTextPaint() {
		if ( mTextPaint == null) {
			mTextPaint = new Paint();
			mTextPaint.setARGB(255, 255, 255, 255);
			mTextPaint.setAntiAlias(true);
		}
		return mTextPaint;
	}
	
	public void  setPOIManager(PointOfInterestManager manager){
		this. POIManager = manager;
	}
}
