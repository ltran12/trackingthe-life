package tracking.life.android;

import java.util.ArrayList;

import tracking.life.android.database.adapter.LocationHistoryDbAdapter;
import tracking.life.android.database.adapter.LocationListDbAdapter;
import tracking.life.android.database.adapter.LocationNoteDbAdapter;
import tracking.life.android.database.adapter.TaskListTableDbAdapter;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

//This class will handle map touches to input new POIs 
public class PointOfInterestManager {

	private GeoPoint GPSPoint;
	public ArrayList<PointOfInterest> PointsOfInterests;
	private Location location;
	private LocationListDbAdapter locationDbAdapter;
	private LocationHistoryDbAdapter locationHistoryDbAdapter;
	private TaskListTableDbAdapter taskListTableDbAdapter;
	private LocationNoteDbAdapter locationNoteDbAdapter;
	private static long id;
	private Context context;
	private long memberId;

	public PointOfInterestManager(Context context, long memId) {
		GPSPoint = new GeoPoint(0, 0);
		memberId = memId;
		PointsOfInterests = new ArrayList<PointOfInterest>();
		this.context = context;
		locationDbAdapter = new LocationListDbAdapter(this.context);
		locationDbAdapter.open();
		locationHistoryDbAdapter = new LocationHistoryDbAdapter(this.context);
		locationHistoryDbAdapter.open();
		locationNoteDbAdapter = new LocationNoteDbAdapter(this.context);
		locationNoteDbAdapter.open();
		taskListTableDbAdapter = new TaskListTableDbAdapter(this.context);
		taskListTableDbAdapter.open();
		updatePOIList();

	}

	public void addPOIHistory(POIHistory point) {

		long locID = searchInLocationList(point.getPointOfInterst()
				.getLocation());

		if (locID < 0) {
			// locationDbAdapter.insertNewLocation(memberId, point
			// .getPointOfInterst().getLocation().getLongitude()
			// + "", point.getPointOfInterst().getLocation().getLatitude()
			// + "");

			locID = locationDbAdapter.insertNewLocationWithName(memberId, point
					.getPointOfInterst().getLocation().getLongitude()
					+ "", point.getPointOfInterst().getLocation().getLatitude()
					+ "", "unknown", 1);
			// System.out.println("dhasf");
			PointsOfInterests.add(point.getPointOfInterst());

		}
		// System.out.println("LocID = 	" + locID);
		System.out.println("hahdhfasdlfasldf as;a lsdf;la jd");
		locationHistoryDbAdapter.insertLocation(memberId, locID,
				(int) point.getLengthVisit(), point.getPointOfInterst()
						.getLocation().getTime());

	}

	public long addNote(Location location, String note) {
		 long id = addPOI(location, "unknown");

		locationNoteDbAdapter.insertNewNote(memberId, id, note);

		return id;
	}

	public PointOfInterest doesPOIExist(Location location) {

		for (PointOfInterest poi : PointsOfInterests) {
			if (equals(location, poi.getLocation())) {
				Log.v("from doesExist", "loction with id: " + poi.getId());
				return poi;
			}
		}
		return null;
	}

	public PointOfInterest getPOIWithId(long id) {

		for (PointOfInterest poi : PointsOfInterests) {
			if (poi.getId() == id) {
				Log.v("from doesExist", "loction with id: " + poi.getId());
				return poi;
			}
		}
		return null;
	}

	public long addPOI(Location location, String name) {
		PointOfInterest point;
		long id = -1;
		point = doesPOIExist(location);

		if (point != null) {
			id = point.getId();
			Log.v("from addPOI...location found", "loction with id: " + id);
		}

		else {
			// need some error checking
			if ((id = locationDbAdapter.insertNewLocationWithName(memberId,
					String.valueOf(location.getLongitude()),
					String.valueOf(location.getLatitude()), name, 1)) != -1) {
				PointOfInterest poi = new PointOfInterest(location);
				poi.setId(id);
				poi.setPOIName(name);
				PointsOfInterests.add(poi);
				Log.v("from addPOI....new location",
						"loction with id: " + poi.getId());

				System.out.println(memberId);

			}
		}

		return id;
	}

	public void removePOI(int id) {

		if (id != -1) {
			locationDbAdapter.deleteByRowId(id);
			updatePOIList();
			Log.v("from removePOI", "loction with id: " + id);
		} else
			Log.v("from removePOI", "loction was not located in db");
	}

	public long getPOIID(Location location) {

		PointOfInterest point;

		point = doesPOIExist(location);

		if (point != null) {
			return point.getId();
		}

		return -1;
	}

	public Location getLocationWithID(long locationID) {
		for (PointOfInterest poi : PointsOfInterests) {
			if (poi.getId() == locationID) {
				return poi.getLocation();
			}
		}
		return null;

	}

	public void changeLocationTitle(long locationId, String title) {

		if (locationDbAdapter.updateLocationTitle(locationId, title)) {
			PointsOfInterests.get((int) locationId).setPOIName(title);
		}
	}

	public void setLocation(long id, Location location) {
		PointsOfInterests.get((int) id).setLocation(location);
	}

	public GeoPoint convertLocationToPoint(Location location) {
		GeoPoint point = new GeoPoint((int) (location.getLatitude() * 1E6),
				(int) (location.getLongitude() * 1E6));
		return point;
	}

	public boolean equals(Location location, Location dbLocation) {
		if (location.distanceTo(dbLocation) < 50) {
			return true;
		}
		return false;
	}

	public static int convertTo1E6(double point) {
		return (int) (point * 1E6);
	}

	private void updatePOIList() {
		Cursor cur = locationDbAdapter.selectAllLocationListByMemberId(
				memberId);
		
		PointsOfInterests.clear();
		PointOfInterest poi;

		if (cur != null && cur.getCount() > 0) {
			int count = cur.getCount();
			float longitude;
			float latitude;
			long thisID;
			String name;
			cur.moveToFirst();
			int y = 0;
			do {
				longitude = cur.getFloat(cur.getColumnIndex("longitude"));
				latitude = cur.getFloat(cur.getColumnIndex("latitude"));
				thisID = cur.getLong(cur.getColumnIndex("_id"));
				name = cur.getString(cur.getColumnIndex("title"));
				Log.v("from updatePOILIst: ", "title: " + name + " longitude: "
						+ longitude + " latitude: " + latitude
						+ " Locationid: " + thisID + " this is the : " + y++
						+ " through the list");
				poi = new PointOfInterest();
				poi.setLatitude(latitude);
				poi.setLongitude(longitude);
				poi.setId(thisID);
				poi.setPOIName(name);
				GeoPoint point = new GeoPoint(convertTo1E6(latitude),
						convertTo1E6(longitude));

				poi.setGeoPoint(point);
				if (!name.equals("unknown"))
					PointsOfInterests.add(poi);

			} while (cur.moveToNext());
		}

		if (cur.getCount() == 0) {
			Log.v("from updatePOIList", "List is empty");
		}
	}

	public void setOverlayItem(long locationId, OverlayItem item) {
		for (PointOfInterest poi : PointsOfInterests) {
			if (poi.getId() == locationId) {
				poi.setOverlayItem(item);
				return;
			}
		}

		Log.v("From setOverlayItem", "Item was not found");
	}

	public long findLoctionWithOverlayItem(OverlayItem item) {
		for (PointOfInterest poi : PointsOfInterests) {
			if (poi.getOverlayItem() == item) {
				return poi.getId();
			}
		}

		return -1;
	}

	private int getIndexOfPOI(int locationID) {

		for (PointOfInterest poi : PointsOfInterests) {
			if (poi.getId() == locationID) {
				return PointsOfInterests.indexOf(poi);
			}
		}
		return -1;
	}

	public long getPOIByTitle(String title) {
		for (PointOfInterest poi : PointsOfInterests) {
			if (poi.getPOIName().compareTo(title) == 0) {
				return poi.getId();
			}
		}

		return -1;
	}

	public long searchInLocationList(Location loc) {
		Cursor cur = locationDbAdapter
				.selectAllLocationListByMemberId(memberId);
		if (cur != null) {
			if (cur.moveToFirst()) {
				do {
					float longt = cur.getFloat(cur.getColumnIndex("longitude"));
					float latt = cur.getFloat(cur.getColumnIndex("latitude"));
					Location lo = new Location("null");
					lo.setLatitude(latt);
					lo.setLongitude(longt);
					System.out.println(lo.distanceTo(loc));
					if (lo.distanceTo(loc) < 50) {
						return cur.getLong(cur.getColumnIndex("_id"));
					}
				} while (cur.moveToNext());

			}
		}

		return -1;
	}

	public String allInList() {
		// Cursor cur = locationDbAdapter.selectAll();
		Cursor cur = locationNoteDbAdapter.selectAll();
		String list = "";
		if (cur != null) {
			if (cur.moveToFirst()) {
				// for (int i = 0; i < cur.getCount(); i++) {
				for (int j = 0; j < cur.getColumnCount(); j++) {
					list += cur.getString(j) + "\t";
				}
				if (!cur.moveToNext())
					// break;
					list += "\n ";
				// }
			}
		}
		// System.out.println(list);

		return list;
	}

	/* return a list of POIHistory by a location */
	public ArrayList<POIHistory> listByLocation(Location loc) {
		ArrayList<POIHistory> list = new ArrayList<POIHistory>();
		long index = searchInLocationList(loc);
		Cursor cur = locationHistoryDbAdapter.selectLocationHistoryByLocation(
				memberId, index);
		if (cur != null) {
			if (cur.moveToFirst()) {
				do {

					POIHistory poi = new POIHistory(new PointOfInterest(loc));
					poi.setLengthVisit(cur.getLong(cur
							.getColumnIndex("timeLength")));
					poi.getPointOfInterst()
							.getLocation()
							.setTime(
									cur.getLong(cur.getColumnIndex("timeStamp")));
					list.add(poi);

				} while (cur.moveToNext());
			}
		}
		return list;

	}

	/* Task list by location */
	public ArrayList<String> listTaskByLocation(Location loc) {

		ArrayList<String> list = new ArrayList<String>();
		Cursor cur = taskListTableDbAdapter.selectAllTaskList(memberId);
		if (cur != null) {
			if (cur.moveToFirst()) {
				do {
					String time = cur
							.getString(cur.getColumnIndex("timeStamp"));
					String task = cur.getString(cur
							.getColumnIndex("taskDetails"));
					long locID = cur.getLong(cur
							.getColumnIndex("locationListId"));

					Cursor cur2 = locationDbAdapter.selectLocationByID(
							memberId, locID);
					String title = "";
					if (cur2 != null) {
						if (cur2.moveToFirst()) {
							title = cur2
									.getString(cur2.getColumnIndex("title"));
						}
					}
					String total = "Time: " + time + "\nPlace: " + title
							+ "\nTask: " + task;
					list.add(total);

				} while (cur.moveToNext());
			}
		}
		return list;

	}

	public ArrayList<String> listNoteByLocation() {
		ArrayList<String> list = new ArrayList<String>();

		Cursor cur = locationNoteDbAdapter
				.selectAllLocationNoteByMemberId(memberId);
		if (cur != null) {
			if (cur.moveToFirst()) {
				do {
					String time = cur
							.getString(cur.getColumnIndex("timeStamp"));
					String task = cur.getString(cur.getColumnIndex("note"));
					long locID = cur.getLong(cur
							.getColumnIndex("locationListId"));

					Cursor cur2 = locationDbAdapter.selectLocationByID(
							memberId, locID);
					String title = "";
					if (cur2 != null) {
						if (cur2.moveToFirst()) {
							title = cur2
									.getString(cur2.getColumnIndex("title"));
						}
					}
					String total = "Place: " + title + "\nNote: " + task;

					list.add(total);

				} while (cur.moveToNext());
			}
		}

		return list;

	}

	public ArrayList<String> listLocation() {
		ArrayList<String> list = new ArrayList<String>();
		Cursor cur = locationDbAdapter
				.selectAllLocationListByMemberId(memberId);

		if (cur != null) {
			if (cur.moveToFirst()) {
				do {

					String longi = cur.getString(cur
							.getColumnIndex("longitude"));
					String lat = cur.getString(cur.getColumnIndex("latitude"));

					String place = cur.getString(cur.getColumnIndex("title"));
					if (place.equals("unknown"))
						continue;
					String total = "Place: " + place + "\n Longitude: " + longi
							+ " Latitude: " + lat + "\n\n";
					list.add(total);
				} while (cur.moveToNext());
			}
		}
		return list;

	}

	/* Calculate statistic of the recent visit location */
	public ArrayList<String> calculateStatistic() {
		ArrayList<String> list = new ArrayList<String>();

		Cursor cur = locationHistoryDbAdapter.selectLocationHistoryByMemberId(memberId);

		String title = null;
		long locL = 0;
		long durL = 0;
		String timeL = "";
		if (cur != null) {
			if (cur.moveToFirst()) {
				String time = cur.getString(cur.getColumnIndex("timeStamp"));

				long duration = cur.getLong(cur.getColumnIndex("timeLength"));
				long id = cur.getLong(cur.getColumnIndex("locationListId"));

				timeL = time;
				locL = id;
				durL = duration;

				while (cur.moveToNext()) {
					time = cur.getString(cur.getColumnIndex("timeStamp"));

					duration = cur.getLong(cur.getColumnIndex("timeLength"));
					id = cur.getLong(cur.getColumnIndex("locationListId"));
					if (duration == 0) {
						continue;
					}
					if (locL == id) {
						durL += duration;
						continue;
					}

					Cursor cur2 = locationDbAdapter.selectLocationByID(
							memberId, id);

					if (cur2 != null) {
						if (cur2.moveToFirst()) {
							title = cur2
									.getString(cur2.getColumnIndex("title"));

						}
					}
					if (title == null)
						title = "unknown place";
					String total = "You have spent " + durL / (1000)
							+ " mins                   \nAt " + title;
					//\nFrom: " + timeL
					list.add(total);

					timeL = time;
					locL = id;
					durL = duration;

				}

				if (list.isEmpty() && locL != 0) {

					String total = "You have spent " + durL / (1000)
							+ " secs \nAt " + title;
					//" \nFrom: " + timeL
					list.add(total);
				}
			}
		}

		return list;

	}
}
