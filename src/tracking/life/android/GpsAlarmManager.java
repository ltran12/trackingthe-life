package tracking.life.android;

import java.util.ArrayList;
import tracking.life.android.database.adapter.PersonalSettingsDbAdapter;
import tracking.life.android.database.adapter.TaskListTableDbAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

/* this class will manage the alarms set by the user
 should it be an activity or a background service
 */
public class GpsAlarmManager {
	private ArrayList<GpsAlarm> gpsAlarms;
	private GpsAlarm gpsAlarm;
	private TaskListTableDbAdapter taskListDbHelper;
	private PersonalSettingsDbAdapter settingsDbHelper;
	private Context context;
	private static long alarmId;
	private static long memberId;

	public GpsAlarmManager(Context context, long memId) {
		gpsAlarm = null;
		gpsAlarms = new ArrayList<GpsAlarm>();
		this.context = context;
		taskListDbHelper = new TaskListTableDbAdapter(this.context);
		taskListDbHelper.open();

		settingsDbHelper = new PersonalSettingsDbAdapter(this.context);
		settingsDbHelper.open();

	
		alarmId = 0;
		memberId = memId;
		updateAlarmList();
	}

	// used when POI is known
	public long addAlarm(long locationId, String task) {
		long newId = -1;

		if (locationId != -1) {
			Log.v("addAlarm", "location is okay");
			newId = taskListDbHelper.insertNewTask(memberId, locationId, task);
			gpsAlarm = new GpsAlarm(newId, locationId, task);
			gpsAlarms.add(gpsAlarm);

		}
		return newId;
	}

	public void addNoteToAlarm(long alarmID, String task) {

		gpsAlarms.get(getIndexOfAlarm((int) alarmID)).addNote(task);

		long locationListId = gpsAlarms.get(getIndexOfAlarm((int) alarmID))
				.getLocationId();

		taskListDbHelper.insertNewTask(memberId, locationListId, task);

	}

	public ArrayList<GpsAlarm> getListOfAlarms() {
		return gpsAlarms;
	}

	public void setIntent(long alarmID, Intent intent) {
		int id = getIndexOfAlarm((int) alarmID);

		gpsAlarms.get(id).setPendingIntent(intent);
	}

	private int getIndexOfAlarm(int alarmID) {
		for (GpsAlarm gps : gpsAlarms) {
			if (gps.getAlarmId() == alarmID) {
				return (int) gpsAlarms.indexOf(gps);
			}
		}
		return -1;
	}

	public GpsAlarm getAlarmByLocation(long locationID) {
		for (GpsAlarm gps : gpsAlarms) {
			if (gps.getLocationId() == locationID) {
				return gps;
			}
		}

		return null;
	}

	private void updateAlarmList() {
		Cursor cur = taskListDbHelper.selectAllTaskList(memberId);

		gpsAlarms.clear();
		int alarmID;
		int locationID;
		String task;
		
		if (cur != null && cur.getCount() > 0) {

			if (cur.moveToFirst()) {
				int y = 0;
				do {
					locationID = cur.getInt(cur
							.getColumnIndex("locationListId"));

					alarmID = cur.getInt(cur.getColumnIndex("_id"));
					task = cur.getString(cur.getColumnIndex("taskDetails"));
					Log.v("from updateAlarmList: ", "alarmID: " + alarmID
							+ " locationID: " + locationID + " "
							+ " this is the : " + y++ + " through the list");
					gpsAlarms.add(new GpsAlarm(alarmId, locationID,
							task));

				} while (cur.moveToNext());
			}

			if (cur.getCount() == 0) {
				Log.v("from updateAlarmList", "List is empty");
			}
		}
	}

}
