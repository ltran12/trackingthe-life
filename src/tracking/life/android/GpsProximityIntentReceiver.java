package tracking.life.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;


// may need to use a local Broadcast receiver to avoid security leaks 
public class GpsProximityIntentReceiver extends BroadcastReceiver{
	
	private long currentLoctionID;
	private long currentAlarmID;
	private long memId;
	
	private NotificationManager notificationManager;
	private Notification updateComplete;
	
	private GpsAlarmManager alarmManager;
	private PointOfInterestManager POIManager;
	private GpsAlarm alarm;
	private PointOfInterest poi;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		
		
		
		String key = LocationManager.KEY_PROXIMITY_ENTERING; 
		Boolean entered  = intent.getBooleanExtra(key, false);
		currentLoctionID = intent.getLongExtra("locationID", -1);
		currentAlarmID   =  intent.getLongExtra("alarmID", -1); 
		
		memId			 =  intent.getLongExtra("mem", 0);
		
		Log.v("from onRecive", "Memid " + memId);
	
		alarmManager     = new GpsAlarmManager(context,memId );
		POIManager       = new PointOfInterestManager(context,memId );
		
		if(entered){
			
			poi   = POIManager.getPOIWithId(currentLoctionID);
			if(poi == null){
				Log.v("from onRecieve", " poi was null and location id : "  + currentLoctionID + "alarmId" + currentAlarmID);
				return;
			}
			alarm = alarmManager.getAlarmByLocation(currentLoctionID); 
			
			Intent i = new Intent("Locaton ID: " + currentLoctionID + "Alarm ID: " + currentAlarmID);
			
			PendingIntent detailsIntent = 
		            PendingIntent.getActivity(context, 0, i, 0);			
			
			//set up a notification
			notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			updateComplete = new Notification();
			updateComplete.icon = android.R.drawable.stat_notify_sync;
			
			updateComplete.tickerText = "You have have a task at " + poi.getPOIName();
			updateComplete.when = System.currentTimeMillis();
			
			CharSequence from = "You have entered POI " + poi.getPOIName();
	        CharSequence message = alarm.getAlarmMessagesInStringFormat();
	        
	        updateComplete.setLatestEventInfo(context, from	, message, detailsIntent);
			notificationManager.notify(1000, updateComplete);
			Log.v("Entered", "You just returned to the location with Id: " + currentLoctionID);
		
		}
		
		else{
			//do nothing????
			Log.v("Leaving", "You just left to the location with Id: " + currentLoctionID);
			// need to remove alarm from db still
			
			// need to recreate the pending intent and remove
		  
		}
		
	}

}
