package tracking.life.android;

public class POIHistory {

	private long lengthVisit;
	private long timeEnd;
	private PointOfInterest point;
	private int count;

	public POIHistory() {
		lengthVisit = 0;
		timeEnd = 0;

	}

	public POIHistory(PointOfInterest point) {
		this.point = point;
		count = 1;
	}

	public void resetCount() {
		count = 0;
	}

	public void addCount() {
		count++;
	}

	public int getCount() {
		return count;
	}

	public POIHistory(PointOfInterest point, long timeEnd, long lengthVisit) {
		this.lengthVisit = lengthVisit;
		this.timeEnd = timeEnd;
		this.point = point;
	}

	public long getLengthVisit() {
		return lengthVisit;
	}

	public void setLengthVisit(long l) {
		lengthVisit = l;
	}

	public void addLength(long t) {
		lengthVisit += t;
	}

	public long getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(long t) {
		timeEnd = t;
	}

	public PointOfInterest getPointOfInterst() {
		return point;
	}

	public void setPointOfInterest(PointOfInterest t) {
		point = t;
	}

	public String toString() {
		return "Loction: " + point.getPOIName() + " at the longtitue of: "
				+ point.getLocation().getLongitude() + " and lat of "
				+ point.getLocation().getLatitude() + " \n " + " for "
				+ lengthVisit;
	}

}
